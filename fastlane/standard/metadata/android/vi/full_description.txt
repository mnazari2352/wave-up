WaveUp là một ứng dụng <i>đánh thức điện thoại của bạn</i> - bật màn hình lên - khi bạn <i>vẫy tay</i> trên cảm biến tiệm cận.

Tôi đã phát triển ứng dụng này vì tôi muốn tránh việc nhấn nút nguồn chỉ để xem đồng hồ - việc mà tôi tình cờ làm rất nhiều trên điện thoại của tôi. Đã có sẵn các ứng dụng khác làm việc này rồi - và còn hơn thế nữa. Tôi được truyền cảm hứng từ Gravity Screen On/Off, đó là một ứng dụng <b>tuyệt vời</b>. Tuy nhiên, tôi là người hâm mộ lớn của phần mềm mã nguồn mở và tôi cố gắng cài đặt phần mềm tự do vào điện thoại nếu có thể. Tôi không thể tìm một ứng dụng mã nguồn mở làm việc này nên tôi tự phát triển nó. Nếu bạn quan tâm, bạn có thể xem mã nguồn:
https://gitlab.com/juanitobananas/wave-up

Chỉ cần vẫy tay trên cảm biến tiệm cận để bật màn hình lên. Cái này được gọi là <i>chế độ vẫy tay</i> và có thể được tắt ở màn hình cài đặt để tránh việc vô tình bật màn hình lên.

Nó cũng sẽ bật màn hình lên khi bạn lấy điện thoại thông minh của bạn ra khỏi túi quần/áo hoặc túi xách. Cái này được gọi là <i>chế độ bỏ túi</i> và cũng có thể được tắt ở màn hình cài đặt.

Cả hai chế độ này được bật theo mặc định.

Nó cũng khoá điện thoại của bạn và tắt màn hình nếu bạn che cảm biến tiệm cận trong một giây (hoặc một thời gian được chỉ định). Cái này không có tên đặc biệt nhưng cũng vẫn có thể được thay đổi ở màn hình cài đặt. Cái này không được bật theo mặc định.

Dành cho những người chưa bao giờ nghe đến cảm biến tiệm cận: nó là một thứ nhỏ ở đâu đó gần chỗ bạn đặt tai của bạn khi bạn nói chuyện điện thoại. Bạn hầu như không thể nhìn thấy nó và nó có trách nhiệm bảo điện thoại của bạn tắt màn hình khi bạn đang trong một cuộc gọi.

<b>Gỡ cài đặt</b>

Ứng dụng này sử dụng quyền Quản trị thiết bị. Vì vậy bạn không thể gỡ cài đặt WaveUp một cách 'bình thường'.

Để gỡ cài đặt nó, chỉ cần mở nó và sử dụng nút 'Gỡ cài đặt WaveUp' ở dưới cùng menu.

<b>Các vấn đề đã biết</b>

Không may mắn thay, một số điện thoại thông minh để CPU chạy trong khi đang lắng nghe cảm biến tiệm cận. Cái này được gọi là <i>wake lock</i> và gây ra hao pin đáng kể. Đây không phải lỗi của tôi và tôi không thể làm gì để thay đổi điều này. Các điện thoại khác sẽ "đi ngủ" khi màn hình được tắt trong khi vẫn đang lắng nghe cảm biến tiệm cận. Trong trường hợp này, hầu như không hao pin. 

<b>Các quyền Android được yêu cầu:</b>

> WAKE_LOCK để bật màn hình lên
> USES_POLICY_FORCE_LOCK để khoá thiết bị
> RECEIVE_BOOT_COMPLETED để tự động chạy sau khi khởi động thiết bị nếu được chọn
> READ_PHONE_STATE để tạm dừng WaveUp khi đang trong cuộc gọi

<b>Các lời nhắc khác</b>

Đây là ứng dụng Android đầu tiên mà tôi từng viết ra, nên hãy cẩn thận!

Đây cũng là sự đóng góp nhỏ của tôi cho thế giới mã nguồn mở. Cuối cùng!

Tôi sẽ rất thích nếu bạn có thể cho tôi bất kỳ loại phản hồi nào hoặc đóng góp theo bất kỳ cách nào!

Cảm ơn vì đã đọc!

Mã nguồn mở thật tuyệt!!!

<b>Dịch</b>

Sẽ thật sự tuyệt nếu bạn có thể giúp dịch WaveUp thành ngôn ngữ của bạn (kể cả phiên bản tiếng Anh cũng có thể được xem xét lại)
Có hai dự án để dịch trên Transifex: https://www.transifex.com/juanitobananas/waveup/ và https://www.transifex.com/juanitobananas/libcommon/.

<b>Sự công nhận</b>

Lời cảm ơn đặc biệt của tôi đến:

Hãy xem: https://gitlab.com/juanitobananas/wave-up/#acknowledgments