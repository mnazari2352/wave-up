New in 3.0.5
★ Fix small bug in settings logic.
★ Add Hebrew and (incomplete) Turkish translations.
★ Update Danish and Japanese translations.

New in 3.0.4
★ Fix small bug that kept Android's volume dialog open for very long.
★ Update Italian and Danish translations.

New in 3.0.3
★ Update Ukrainian translation. Thanks, Vlad!
★ Update Persian translation. Thanks, Moh!
★ Fix crash in some Huawei devices (excluding apps from locking might not work yet though).
★ Fix small bug in settings logic.
