New in 3.0.9
★ Work-around for small bug showing a "gratitude" dialog that shouldn't be shown under some circumstances

New in 3.0.8
★ Fix crash on some devices that can't write log to cache dir.
★ Possibly fix problem while attaching logs to send with email app.

New in 3.0.7
★ Fix small error reporting bugs.
