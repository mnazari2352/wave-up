/*
 * Copyright (c) 2016-2019 Juan García Basilio
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup.service

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import com.jarsilio.android.common.utils.SingletonHolder
import com.jarsilio.android.waveup.extensions.settings
import com.jarsilio.android.waveup.extensions.state
import com.jarsilio.android.waveup.prefs.Settings
import timber.log.Timber

class ProximitySensorHandler private constructor(val context: Context) : SensorEventListener {
    private val applicationContext: Context = context.applicationContext
    private val state: WaveUpWorldState = context.state
    private val settings: Settings = context.settings

    private val sensorManager: SensorManager = applicationContext.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    private val proximitySensor: Sensor? = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY)

    private val screenHandler: ScreenHandler = ScreenHandler.getInstance(applicationContext)

    private var lastDistance = Distance.FAR
    private var lastTime: Long = 0

    private var waveCount = 0
    private var lastWaveTime: Long = 0

    private var listening = false

    private enum class Distance {
        NEAR, FAR
    }

    fun isProximitySensorAvailable(): Boolean {
        return sensorManager != null && proximitySensor != null
    }

    private fun start() {
        if (!isProximitySensorAvailable()) {
            Timber.e("Failed to read from proximity sensor. Does the device have one?")
            return
        }

        if (!listening) {
            Timber.d("Registering proximity sensor listener.")
            sensorManager.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL)
            listening = true
        } else {
            Timber.d("Proximity sensor listener is already registered. There is no need to register it again.")
        }
    }

    fun startOrStopListeningDependingOnConditions() {
        Timber.v("Current state: $state")
        Timber.v("Current settings: $settings")
        val startAllowedByWaveOrLockModes = !state.isScreenOn && (settings.isPocketMode || settings.isWaveMode) ||
            state.isScreenOn && settings.isLockScreen && (state.isLockScreenAdmin || settings.isLockScreenWithPowerButton || state.isAccessibilityServiceEnabled)
        val startAllowedByOrientation = settings.isLockScreenWhenLandscape || state.isPortrait || !state.isScreenOn
        val startAllowedByNoOngoingCall = !state.isOngoingCall ||
            state.isHeadsetConnected || state.isBluetoothHeadsetConnected // If headset is connected, continue running

        Timber.v("Start because of wave or lock modes? $startAllowedByWaveOrLockModes")
        Timber.v("Start because of orientation? $startAllowedByOrientation")
        Timber.v("Start because of no ongoing call (or a headset or a bluetooth headset is being used to call)? $startAllowedByNoOngoingCall")

        val start = startAllowedByWaveOrLockModes && startAllowedByOrientation && startAllowedByNoOngoingCall

        if (start) {
            Timber.d("Starting because an event happened and the world in combination with the settings say I should start listening")
            start()
        } else {
            Timber.d("Stopping because an event happened and the world in combination with the settings say I should stop listening")
            stop()
        }
    }

    fun stop() {
        screenHandler.cancelTurnOff()

        if (!isProximitySensorAvailable()) {
            Timber.e("Failed to read from proximity sensor. Does the device have one?")
            return
        }

        if (listening) {
            Timber.d("Unregistering proximity sensor listener")
            sensorManager.unregisterListener(this)
            listening = false
        } else {
            Timber.d("Proximity sensor listener is already unregistered. There is no need to unregister it again.")
        }
    }

    override fun onSensorChanged(event: SensorEvent) {
        val currentTime = System.currentTimeMillis()
        val currentDistance = if (event.values[0] >= event.sensor.maximumRange) Distance.FAR else Distance.NEAR
        Timber.v("Proximity sensor changed: $currentDistance (current sensor value: ${event.values[0]} - max. sensor value: ${event.sensor.maximumRange})")

        // If the sensor gets uncovered, there is possibly a thread waiting to turn off the screen. It needs to be interrupted.
        if (currentDistance == Distance.FAR) {
            screenHandler.cancelTurnOff()
        }

        val uncovered = lastDistance == Distance.NEAR && currentDistance == Distance.FAR
        val covered = lastDistance == Distance.FAR && currentDistance == Distance.NEAR

        val timeBetweenFarAndNear = currentTime - lastTime
        if (uncovered) {
            Timber.v("Just uncovered. Time it was covered: $timeBetweenFarAndNear")
        } else {
            Timber.v("Just covered. Time it was uncovered: $timeBetweenFarAndNear")
        }

        val waved = timeBetweenFarAndNear <= WAVE_THRESHOLD
        val tookOutOfPocket = timeBetweenFarAndNear > POCKET_THRESHOLD

        if (uncovered) {
            val timeSinceLastScreenOnOrOff = currentTime - screenHandler.lastTimeScreenOnOrOff
            if (timeSinceLastScreenOnOrOff > MIN_TIME_BETWEEN_SCREEN_ON_AND_OFF) { // Don't do anything if it turned on or off 1.5 seconds ago
                if (waved && settings.isWaveMode) {
                    if (currentTime - lastWaveTime > WAVE_THRESHOLD) {
                        waveCount = 0 // Reset wave count the last wave was a long time ago (in this case 2 seconds) - Will only switch on screen if waves happen within 2 seconds
                    }

                    waveCount++
                    Timber.v("Waved. waveCount: $waveCount")
                    Timber.v("Time between waves was: ${currentTime - lastWaveTime} (will only switch on screen if waves happen within 2 seconds)")
                    lastWaveTime = System.currentTimeMillis()
                    val minWaves = settings.numberOfWavesToWaveUp - 1
                    if (waveCount > minWaves) {
                        screenHandler.turnOnScreen()
                        waveCount = 0
                    }
                } else if (tookOutOfPocket && settings.isPocketMode) {
                    screenHandler.turnOnScreen()
                }
            } else {
                Timber.d("Time since last screen off: $timeSinceLastScreenOnOrOff. Not switching it on")
            }
        } else if (covered) {
            screenHandler.turnOffScreen()
        }

        lastDistance = currentDistance
        lastTime = currentTime
    }

    override fun onAccuracyChanged(sensor: Sensor, i: Int) {}

    companion object : SingletonHolder<ProximitySensorHandler, Context>(::ProximitySensorHandler) {
        private const val WAVE_THRESHOLD: Long = 2000
        private const val POCKET_THRESHOLD: Long = 5000
        private const val MIN_TIME_BETWEEN_SCREEN_ON_AND_OFF: Long = 1500
    }
}
