<resources>
    <string name="prefs_service">Service</string>
    <string name="pref_enable">Enable</string>

    <string name="prefs_modes">WaveUp modes</string>
    <string name="pref_wave_mode">Wave mode</string>
    <string name="pref_wave_mode_summary">Turn on screen when you wave over the proximity sensor.</string>
    <string name="pref_pocket_mode">Pocket mode</string>
    <string name="pref_pocket_mode_summary">Turn on screen when you take your phone out of your pocket or purse.</string>

    <string name="pref_lock_screen">Lock screen</string>
    <string name="pref_lock_screen_mode_summary">Turn off and lock screen when the proximity sensor is covered.</string>
    <string name="pref_lock_screen_when_landscape">Lock in landscape mode</string>
    <string name="pref_lock_screen_when_landscape_summary">Disable this to prevent locking when holding the device horizontally to view media for example.</string>
    <string name="pref_lock_screen_with_power_button_as_root">Work-around for Fingerprint and Smart Lock</string>
    <string name="pref_lock_screen_with_power_button_as_root_summary">Turn off the screen simulating a power button press. Requires root.</string>
    <string name="pref_lock_screen_vibrate_on_lock">Vibrate before locking</string>
    <string name="pref_lock_screen_vibrate_on_lock_summary">Notifies you of detection of proximity to avoid locking the phone accidentally.</string>
    <string name="pref_lock_screen_app_exception">Excluded apps</string>
    <string name="pref_lock_screen_app_exception_summary">Prevent locking if one of these apps is in the foreground. Tap to open and edit list.</string>
    <string name="pref_sensor_cover_time_before_locking_screen">Cover time before locking</string>
    <string name="pref_sensor_cover_time_before_locking_screen_summary">Period of time proximity sensor has to be covered before locking. Current value: %s.</string>
    <string name="pref_notification_section">Notification</string>
    <string name="pref_show_notification">Show notification</string>
    <string name="pref_show_notification_summary">Show a persistent notification to manage WaveUp and to make sure it stays alive.</string>
    <!-- From Pie onwards, we just show the Android system settings and always use a ForegroundService -->
    <string name="pref_show_notification_v28">Notification settings</string>
    <string name="pref_show_notification_summary_v28">Tap here to open Android\'s notification settings for WaveUp.\n\n<small>Note from the developer: from Android 9 onwards, apps that read sensors in the background are not allowed to <b>not</b> show a permanent notification. However, you can hide it yourself using the system settings. Depending on your device, this might not work. In this case, please switch it on and try again.</small></string>

    <string name="wave_up_service_started">WaveUp is running</string>
    <string name="wave_up_service_stopped">WaveUp is not running</string>
    <string name="tap_to_open">Tap to open WaveUp</string>
    <string name="pause">Pause</string>
    <string name="resume">Resume</string>
    <string name="disable">Stop</string>

    <string name="lock_admin_rights_explanation"><b>Lock device: Device Administrator</b>\n\nIn order to lock the screen, we need to set WaveUp as a Device Administrator.\n\nOnce an app is a Device Administrator, you won\'t be able to uninstall it normally without revoking the Device Admin Permission.\n\nIf you wish to revoke the Device Admin Permission, simply go to <i>Settings → Security → Device Administrators</i> and uncheck WaveUp.\n\nIf you wish to uninstall WaveUp just tap on the \'Uninstall WaveUp\' button at the bottom of the app which revokes the Device Admin Privilege <i>and</i> directly uninstalls WaveUp.\n\nAre you sure you want to set WaveUp as Device Administrator?</string>
    <string name="accessibility_service_explanation">
        <b>Lock device: Accessibility Service</b>
        \n\nIn order to lock the screen, we need to set WaveUp as an \'Accessibility Service\'.
        \n\nGranting an app Accessibility Service allows it to spy on the user if it wants to.
        It also allows an app to use specific functions that wouldn\'t be available if not. For example
        WaveUp uses the AccessibilityService.<i>GLOBAL_ACTION_LOCK_SCREEN</i> to lock the screen.
        \n\n<b>Please think twice before trusting apps!</b>
        \n\nWaveUp is open source software so please go ahead and check the code (or ask a friend to
        do it) to make sure we are not doing anything evil (we promise we are not, but you really
        shouldn\'t take our word for it).
        \n\nDo you want to set WaveUp as an Accessibility Service? You\'ll have to grant the privileges
        manually in the next screen.
    </string>
    <string name="sd_card_lock_admin_rights_not_possible_explanation">
        <b>Error: WaveUp installed on the SD card</b>
        \n\nUnfortunately, it is not possible for apps installed on the SD card to request
        admin privileges, which are needed in order to lock the device.
        \n\n<b>Please move WaveUp to the internal storage and try again.</b>
    </string>

    <string name="something_went_wrong">Something went wrong</string>
    <string name="lock_disabled_warning_device_admin_text">WaveUp had to disable the \'lock\' option due to a missing <i>Device Admin</i> privilege.\n\nPlease re-enable the option and set WaveUp as a <i>Device Admin</i> when prompted.\n\nIf this doesn\'t work please go to <i>Settings → Security → Device Admin Apps</i> and enable WaveUp.</string>
    <string name="lock_disabled_warning_accessibility_settings_text">WaveUp had to disable the \'lock\' option due to a missing <i>Accessibility Service</i> permission.\n\nPlease re-enable the option and set WaveUp as an <i>Accessibility Service</i> when prompted.\n\nIf this doesn\'t work please go to <i>Settings → Accessibility</i> and enable \'WaveUp Lock\'.</string>

    <string name="root_access_failed">Couldn\'t get root access</string>

    <string name="uninstall_button">Uninstall WaveUp</string>
    <string name="removed_device_admin_rights">Removed admin rights. Just activate the \'Lock screen\' option if you wish to grant these again</string>

    <!-- Phone permission dialog -->
    <string name="phone_permission_yes">Request</string>
    <string name="phone_permission_no">Ignore</string>
    <string name="phone_permission_explanation"><b>Phone Permission</b>\n\nWaveUp uses this permission exclusively to know whether there is an ongoing call in order to deactivate itself during the call.\n\nNo information related to the call is queried nor stored. Also the fact that there is an ongoing call is only known by the WaveUp app and is never sent elsewhere.\n\n<b>WaveUp will never make or manage phone calls.</b>\n\nWe recommend that you allow it, but it\'s okay if you don\'t.</string>

    <!-- I put this array here instead of in arrays.xml because it has to be translated. -->
    <string-array name="sensor_cover_time_entries">
        <item>Immediately</item>
        <item>0.5 seconds</item>
        <item>1 second</item>
        <item>1.5 seconds</item>
        <item>2 seconds</item>
        <item>5 seconds</item>
    </string-array>

    <!-- Vibrate on lock -->
    <string name="pref_vibrate_on_lock_time_title">Vibrate before locking</string>
    <string name="pref_vibrate_on_lock_time_title_summary">Notifies you of detection of proximity to avoid locking the phone accidentally.\n\nCurrent vibration time: %s</string>

    <string-array name="vibrate_on_lock_time_entries">
        <item>Do not vibrate</item>
        <item>10 ms</item>
        <item>20 ms</item>
        <item>50 ms</item>
        <item>200 ms</item>
        <item>1000 ms</item>
    </string-array>

    <string name="pref_number_of_waves">Number of waves</string>
    <string name="pref_number_of_waves_summary">Number of times you have to wave over the proximity sensor (cover and uncover) to wake up your device.\n\nCurrent value: %s.</string>

    <string-array name="number_of_waves_entries">
        <item>1 (cover, uncover)</item>
        <item>2 (cover, uncover, cover, uncover)</item>
        <item>3 (cover, uncover, cover, uncover, cover, uncover)</item>
    </string-array>

    <string name="privacy_policy_menu_item">Privacy Policy</string>
    <string name="privacy_policy_phone_permission" translatable="false">
        <![CDATA[
            <h2>Why does WaveUp request \'Phone\' permissions? Will WaveUp \'make and manage phone calls\'?</h2>
            <p>
                No. WaveUp will never make or manage phone calls. It also never knows with whom you speak on the
                phone. It uses this permission exclusively to know whether there is an ongoing call in order to
                deactivate itself during the call. No information related to the call is queried nor stored. Also
                the fact that there is an ongoing call is only known by the WaveUp app and is never sent elsewhere.
            </p>
        ]]>
    </string>

    <string name="licenses_menu_item">Licenses</string>
    <string name="licenses_about_libraries_text" translatable="false"><![CDATA[<br/><b>WaveUp License</b><br/><br/>Copyright (c) 2016-2020 Juan García Basilio<br/><br/>This app is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.<br/><br/>This app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the <a href="http://www.gnu.org/licenses/">GNU General Public License</a> for more details.<br/><br/><b>Third-party libraries</b><br/><br/>Here is a list of the libraries used by this app. Tap on them for more information.]]></string>

    <!-- Excluded apps from locking -->
    <string name="untitled_app">Untitled app</string>
    <string name="exclude_apps_activity_title">Exclude apps</string>
    <string name="excluded_apps">Excluded apps</string>
    <string name="not_excluded_apps">Not excluded apps</string>
    <string name="exclude_apps_text">
        There are no excluded apps at the moment.
        \n\nThe apps listed here will be excluded and WaveUp will not lock
        the screen if any of these is open.
        \n\nJust tap on an app below to add it to this list. Tap on it again
        to remove it if you change your mind.
    </string>
    <string name="usage_access_explanation">
        <b>Exclude apps from locking: Usage Access</b>
        \n\nIn order to <i>not</i> lock the screen if a specific app is running, WaveUp needs to have <i>Usage Access</i>.
        \n\nDo you want grant WaveUp Usage Access? You\'ll have to grant the privileges manually in the next screen.
    </string>


    <!-- Accessibility Service -->
    <string name="accessibility_service_text" translatable="false">WaveUp Lock Command</string>

    <!-- App shortcut -->
    <string name="lock_now">Lock now</string>

    <!-- Lack of proximity sensor -->
    <string name="missing_proximity_sensor_title">Missing proximity sensor</string>
    <string name="missing_proximity_sensor_text">
        WaveUp can\'t run without a proximity sensor. Either your device doesn\'t have one
        or WaveUp isn\'t able to access it. Sorry!
    </string>
</resources>
